package pentastagiu.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import pentastagiu.model.Employee;

public class EmployeeDAO {
	private EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("employee-persistence");
	private EntityManager entityManager = entityManagerFactory.createEntityManager();

	public Employee save(Employee employee) {
		entityManager.getTransaction().begin();
		entityManager.persist(employee);
		entityManager.getTransaction().commit();
		return employee;
	}

	public Employee update(Employee employee) {
		entityManager.getTransaction().begin();
		Employee updatedEmployee = entityManager.merge(employee);
		entityManager.getTransaction().commit();
		return updatedEmployee;
	}

	public Long delete(Long employeeId) {
		entityManager.getTransaction().begin();
		Employee employee = (Employee) entityManager.find(Employee.class, employeeId);
		entityManager.remove(employee);
		entityManager.getTransaction().commit();
		return employeeId;
	}

	public Employee findOneById(Long employeeId) {
		entityManager.getTransaction().begin();
		Employee employee = (Employee) entityManager.find(Employee.class, employeeId);
		entityManager.getTransaction().commit();
		return employee;
	}

	public List<Employee> findOneByName(String employeeName) {
		Query query = entityManager
				.createQuery("SELECT e FROM Employee e WHERE name = '" + employeeName + "'");
		entityManager.getTransaction().begin();
		List<Employee> employeeList = query.getResultList();
		entityManager.getTransaction().commit();
		return employeeList;
	}

	/*
	 * public List<Employee> listAll() { entityManager.getTransaction().begin();
	 * List<Employee> listAllEmployees= entityManager.f }
	 */
}
