package pentastagiu.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Department {
	@Id
	@GeneratedValue
	private Long id_department;
	private String departmentName;

	public Long getId_department() {
		return id_department;
	}

	public void setId_department(Long id_department) {
		this.id_department = id_department;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

}
