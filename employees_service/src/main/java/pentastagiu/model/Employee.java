package pentastagiu.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Employee {
	@Id
	@GeneratedValue
	private Long id_employee;
	private String name;

	@ManyToOne
	private Department department;
	@OneToOne
	private SocialId socialId;
	@ManyToMany
	private Set<Project> projects;

	public Long getId() {
		return id_employee;
	}

	public void setId(Long id) {
		this.id_employee = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public SocialId getSocialId() {
		return socialId;
	}

	public void setSocialId(SocialId socialId) {
		this.socialId = socialId;
	}

	public Set<Project> getProjects() {
		return projects;
	}

	public void setProjects(Set<Project> projects) {
		this.projects = projects;
	}

}
