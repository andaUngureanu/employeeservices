package pentastagiu.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Project {
	@Id
	@GeneratedValue
	private Long id_project;
	private String projectName;

	public Long getId_project() {
		return id_project;
	}

	public void setId_project(Long id_proejct) {
		this.id_project = id_proejct;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
}
