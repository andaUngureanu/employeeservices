package pentastagiu.webservices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

import pentastagiu.model.Employee;
import pentastagiu.services.EmployeeService;

@WebServlet(urlPatterns = { "/employee" })
public class EmployeeWebService extends HttpServlet {

	@Inject
	private EmployeeService employeeService;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
		String json = "";
		String thisLine;
		if (br != null) {
			while ((thisLine = br.readLine()) != null) {
				json = json.concat(thisLine);
			}
		}

		List<Employee> employeeList = new ArrayList<Employee>();
		ObjectMapper mapper = new ObjectMapper();
		Employee employee = mapper.readValue(json, Employee.class);
		employeeService.save(employee);
		response.setContentType("application/json");
		mapper.writeValue(response.getOutputStream(), employee);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		String url = request.getParameter("id");
		Long id = new Long(Integer.parseInt(url));
		Employee employee = employeeService.findOneById(id);
		PrintWriter out = response.getWriter();
		out.println("<html>");
		out.println("<head><title>FindOneById</title></head>");
		out.println("<body>");
		out.println("<p>");
		out.println("Employee details:<br>");
		out.println("Id: " + employee.getId() + "<br>");
		out.println("Name: " + employee.getName() + "<br>");
		out.print("Detapartment name: ");
		if (employee.getDepartment() != null) {
			out.println(employee.getDepartment().getDepartmentName() + "<br>");
		} else
			out.print("\n<br>");
		out.print("Social id: ");
		if (employee.getSocialId() != null) {
			out.println(employee.getSocialId().getIdentifier() + "<br>");
		} else
			out.print("\n<br>");
		out.print("Social id document type: ");
		if (employee.getSocialId() != null) {
			out.println(employee.getSocialId().getDocumentType() + "<br>");
		} else
			out.print("\n<br>");
		out.print("Projects: ");
		if (employee.getDepartment() != null) {
			out.println(employee.getProjects() + "<br>");
		} else
			out.print("\n<br>");
		out.println("</p></body></html>");
		out.close();
	}

}