package pentastagiu.services;

import java.util.List;

import pentastagiu.model.Employee;

public interface EmployeeService {
	public Employee save(Employee employee);

	public List<Employee> findOneByName(String employeeName);

	public Employee findOneById(Long employeeId);
}
