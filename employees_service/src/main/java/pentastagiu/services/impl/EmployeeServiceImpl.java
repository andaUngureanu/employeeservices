package pentastagiu.services.impl;

import java.util.List;

import javax.inject.Inject;

import pentastagiu.dao.EmployeeDAO;
import pentastagiu.model.Employee;
import pentastagiu.services.EmployeeService;

public class EmployeeServiceImpl implements EmployeeService {
	@Inject
	EmployeeDAO employeeDAO;

	public Employee save(Employee employee) {

		return employeeDAO.save(employee);
	}

	public List<Employee> findOneByName(String employeeName) {
		return employeeDAO.findOneByName(employeeName);
	}

	public Employee findOneById(Long employeeId) {
		return employeeDAO.findOneById(employeeId);
	}

}
